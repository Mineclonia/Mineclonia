---

name: "Feature Request"
about: "Mineclonia doesn't do something you need it to"
labels:
 - "feature request"

---

##### Problem

<!--
Describe what's wrong.

If you're reporting a missing feature from Minecraft,
please include a link to the Minetest wiki or Mojang bug
tracker entry describing that feature.
-->

##### Solution

<!-- Write down an example of what you'd like to happen. -->
